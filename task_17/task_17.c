#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
void main()
{
	char *s="This is text";
	char *name="FIFO.fifo";
	char string1[12]="000000000000";
	char *string;
	int size,i,result,p1[2],p2[2],fd,l=strlen(s), f=open("output.txt",O_WRONLY);
	umask(0);
	write(f,s,l);
	close(f);
	f=open("input.txt",O_RDONLY);
	read (f,s,l);
	printf("%s\n",s);
	close(f);
	if(mknod(name,S_IFIFO | 0666,0)<0)
	{
		printf("Error: Can't create FIFO.\n");
	}
	if(0>pipe(p1))
	{
		printf("Error: Can't create pipe\n");
	}
	if(0>pipe(p2))
	{
		printf("Error: Can't create pipe\n");
	}
	int pid=fork();
	if(-1==pid)
	{
		printf("Error: Can't create child\n");
	}
	else if(0<pid)
	{
		close(p1[0]);
		write(p1[1],s,l);
		close(p1[1]);
		close(p2[1]);
		read(p2[0],string1,12);
		printf("read from pipe2: %s\n",string1);
		close(p2[0]);
		if((fd=open(name,O_WRONLY))<0)
		{
			printf("Error: Can't open FIFO for writting.\n");
		}
		write(fd,s,13);
		close(fd);
		printf("Parent exit.\n");
	}
	else
	{
		close(p1[1]);
		read(p1[0],s,l);
		close(p1[0]);
		close(p2[0]);
		write(p2[1],s,l);
		close(p2[1]);
		printf("Read from pipe1: %s\n",s);
		if((fd=open(name,O_RDONLY))<0)
		{
			printf("Error: Can't open FIFO for reading.\n");
		}
		read(fd,string,13);
		printf("Read from FIFO: %s\n",string);
		close(fd);
	}
	unlink(name);
}
